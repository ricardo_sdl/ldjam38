package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.group.FlxGroup;

class VomitEnemy extends Enemy
{
	
	public static inline var WIDTH = 16;
	public static inline var HEIGHT = 16;
	
	public static inline var SPEED = 75;
	
	private var enemiesProjectiles:FlxTypedGroup<EnemyProjectile>;
	
	public var attackTimer = 0.0;
	public var timeToAttack = 0.0;
	public var minTimeToAttack = 1.5;
	public var maxTimeToAttack = 5.0;
	
	
	public function new(x:Float, y:Float, state:Int, player:Player, _enemiesProjectiles:FlxTypedGroup<EnemyProjectile>)
	{
		super(x, y, state, player);
		
		loadGraphic(AssetPaths.vomit_enemy__png);
		
		enemiesProjectiles = _enemiesProjectiles;
		
		health = 50;
		
	}
	
	override public function think(elapsed):Void
	{
		switch state
		{
			case Enemy.STATE_IDLE:
				if (isPlayerNearby())
				{
					state = Enemy.STATE_ATTACKING;
				}
			case Enemy.STATE_ATTACKING:
				if (attackTimer == 0.0)
				{
					timeToAttack = Util.randomBetween(minTimeToAttack, maxTimeToAttack);
				}
				
				attackTimer += elapsed;
				if (attackTimer >= timeToAttack)
				{
					attack();
					attackTimer = 0.0;
					if (!isPlayerNearby())
					{
						state = Enemy.STATE_IDLE;
					}
				}
				
				
				
		}
	}
	
	public function attack():Void
	{
		var midPoint = FlxPoint.get();
		getMidpoint(midPoint);
		var dx = player.x - midPoint.x;
		var dy = player.y - midPoint.y;
		
		var angle = Math.atan2(dy, dx);
		
		var vomit = new VomitProjectile(midPoint.x, midPoint.y, angle, player.x, player.y);
		enemiesProjectiles.add(vomit);
		
		midPoint.put();
		
	}
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
}
