package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;

class PlayerBullet extends FlxSprite
{
	
	public static inline var WIDTH = 4;
	public static inline var HEIGHT = 4;
	
	public static inline var SPEED = 500;
	
	
	public function new(x:Float, y:Float, direction:Int)
	{
		super(x, y, AssetPaths.hero_bullet__png);
		switch (direction)
		{
			case FlxObject.UP:
				velocity.y = - SPEED;
			case FlxObject.DOWN:
				velocity.y = SPEED;
			case FlxObject.LEFT:
				velocity.x = - SPEED;
			case FlxObject.RIGHT:
				velocity.x = SPEED;
		}
	}
	
	
	
	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}
	
	
	
}
