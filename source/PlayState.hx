package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.group.FlxGroup;

class PlayState extends FlxState
{
	
	private var player:Player;
	
	private var enemies:FlxTypedGroup<Enemy>;
	
	private var playerBullets:FlxTypedGroup<PlayerBullet>;
	private var enemiesProjectiles:FlxTypedGroup<EnemyProjectile>;
	
	override public function create():Void
	{
		super.create();
		bgColor = 0xffffffff;
		
		playerBullets = new FlxTypedGroup<PlayerBullet>();
		enemiesProjectiles = new FlxTypedGroup<EnemyProjectile>();
		
		createPlayer(playerBullets);
		add(player);
		
		enemies = new FlxTypedGroup<Enemy>();
		add(enemies);
		
		var spearEnemy = new SpearEnemy(300, 50, Enemy.STATE_IDLE, player);
		enemies.add(spearEnemy);
		
		add(playerBullets);
		add(enemiesProjectiles);
	}
	
	private function createPlayer(playerBullets:FlxTypedGroup<PlayerBullet>):Void
	{
		player = new Player(FlxG.width / 2, FlxG.height / 2, playerBullets);
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		FlxG.overlap(enemiesProjectiles, player, playerHit);
		FlxG.overlap(enemies, player, playerHitByEnemy);
		
	}
	
	public function playerHit(enemmyProjectile, player):Void
	{
		player.hurt(enemmyProjectile.damage);
		enemmyProjectile.kill();
	}
	
	public function playerHitByEnemy(enemy, player):Void
	{
		if (Std.is(enemy, SpearEnemy))
		{
			if (enemy.damageTimer <= 0)
			{
				player.hurt(enemy.damage);
				enemy.damageTimer = enemy.timeWithoutDamage;
			}
			
		}
		
		
	}
	
}
