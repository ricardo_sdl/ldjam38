package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.group.FlxGroup;

class Player extends FlxSprite
{
	
	public static inline var WIDTH = 16;
	public static inline var HEIGHT = 16;
	
	public static inline var SPEED = 150;
	
	private var _playerBullets:FlxTypedGroup<PlayerBullet>;
	
	public var shootTimeIntervalInMs = 100.0;
	public var currentShootTimeInterval = 0.0;
	
	public var midPoint:FlxPoint;
	
	
	public function new(x:Float, y:Float, playerBullets:FlxTypedGroup<PlayerBullet>)
	{
		super(x, y);
		
		loadGraphic(AssetPaths.hero__png, true, WIDTH, HEIGHT);
		animation.add('run', [0, 1, 2, 3, 4], 10);
		animation.add('still', [0], 10);
		
		_playerBullets = playerBullets;
		
		health = 100;
		
		midPoint = new FlxPoint();
		
		//drag.set(50, 50);
		
	}
	
	public function shoot(direction:Int)
	{
		if (currentShootTimeInterval > 0)
		{
			return;
		}
		
		var playerBullet = new PlayerBullet(midPoint.x, midPoint.y, direction);
		_playerBullets.add(playerBullet);
		currentShootTimeInterval = shootTimeIntervalInMs;
		
	}
	
	private function keyBoardInput():Void
	{
		#if !FLX_NO_KEYBOARD
		if (FlxG.keys.anyPressed([LEFT]))
		{
			goLeft();
		}
		else if (FlxG.keys.anyPressed([RIGHT]))
		{
			goRight();
		}
		
		if (FlxG.keys.anyPressed([UP]))
		{
			goUp();
		}
		else if (FlxG.keys.anyPressed([DOWN]))
		{
			goDown();
		}
		
		if (FlxG.keys.pressed.A)
		{
			shoot(FlxObject.LEFT);
		}
		else if (FlxG.keys.pressed.D)
		{
			shoot(FlxObject.RIGHT);
		}
		else if (FlxG.keys.pressed.W)
		{
			shoot(FlxObject.UP);
		}
		else if (FlxG.keys.pressed.S)
		{
			shoot(FlxObject.DOWN);
		}
		
		#end
	}
	
	override public function update(elapsed:Float):Void
	{
		velocity.set(0, 0);
		keyBoardInput();
		if (velocity.x == 0 && velocity.y == 0)
		{
			animation.play('still');
		}
		else
			animation.play('run');
			
		if (currentShootTimeInterval > 0)
		{
			currentShootTimeInterval -= elapsed * 1000;
		}
		
		super.update(elapsed);
		
		getMidpoint(midPoint);
		
	}
	
	public function goLeft():Void
	{
		velocity.x = -1 * SPEED;
	}
	
	public function goRight():Void
	{
		velocity.x = SPEED;
	}
	
	public function goUp():Void
	{
		velocity.y = -1 * SPEED;
	}
	
	public function goDown():Void
	{
		velocity.y = SPEED;
	}
	
	
	
}
