package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.group.FlxGroup;

class VomitProjectile extends EnemyProjectile
{
	
	public static inline var SPEED = 30;
	public static inline var DAMAGE = 15;
	
	public var targetX:Float;
	public var targetY:Float;
	
	public function new(x:Float, y:Float, angle:Float, targetX:Float, targetY:Float)
	{
		super(x, y, DAMAGE);
		
		loadGraphic(AssetPaths.vomit_projectile__png, true);
		animation.add('default', [0, 1, 2, 3], 7);
		animation.play('default');
		velocity.set(Math.cos(angle) * SPEED, Math.sin(angle) * SPEED);
		
		this.targetX = targetX;
		this.targetY = targetY;
		
	}
	
	override public function update(elapsed:Float)
	{
		var dx = targetX - x;
		var dy = targetY - y;
		
		var distance = Math.sqrt(dx * dx + dy * dy);
		if (Util.equalFloats(distance, 1, 1.0))
		{
			kill();
		}
		
		super.update(elapsed);
		
	}
	
}
