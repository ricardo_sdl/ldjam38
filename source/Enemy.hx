package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.group.FlxGroup;

class Enemy extends FlxSprite
{
	
	public static inline var STATE_IDLE:Int = 1;
	public static inline var STATE_RUNNING_AWAY:Int = 2;
	public static inline var STATE_ATTACKING:Int = 4;
	
	public var state:Int;
	
	public var player:Player;
	
	//the default minimum distance that we consider the player as near the enemy
	public var nearbyPlayerDistance:Float;
	
	//if the enemy itself causes damage, set the damage value here
	public var damage = 0.0;
	
	
	
	public function new(x:Float, y:Float, _state:Int, _player:Player)
	{
		state = _state;
		player = _player;
		
		nearbyPlayerDistance = Math.sqrt(FlxG.width * FlxG.width + FlxG.height * FlxG.height) / 2 * 0.5;
		
		super(x, y);
	}
	
	public function think(elapsed:Float):Void
	{
		
	}
	
	public function playerDistance():Float
	{
		var dx = x - player.x;
		var dy = y - player.y;
		
		return Math.sqrt(dx * dx + dy * dy);
		
	}
	
	public function isPlayerNearby(?distance:Float):Bool
	{
		if (distance == null) return playerDistance() <= nearbyPlayerDistance;
		return playerDistance() <= distance;
	}
	
	override public function update(elapsed:Float):Void
	{
		think(elapsed);
		super.update(elapsed);
	}
	
}
