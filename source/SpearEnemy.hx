package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.group.FlxGroup;

class SpearEnemy extends Enemy
{
	
	public static inline var WIDTH = 16;
	public static inline var HEIGHT = 16;
	
	public static inline var SPEED = 100;
	public static inline var ATTACK_SPEED = 200;
	
	public var currentSpeed:Float;
	
	public var minWalkingDistance:Float = 50;
	public var maxWalkingDistance:Float = 150;
	public var currentWalkingDistance:Float = 0.0;
	
	//related to idle state
	public var reachedGoal:Bool = true;
	public var xGoal:Float;
	public var directionToGo:Int;//1 == left, 2 == right
	
	//related to attacking state
	public var targetX:Float;
	public var targetY:Float;
	public var attacking = false;
	public var attackTimer = 0.0;
	
	public var timeWithoutDamage = .5;//the time that the enemy attacks won't have any effect, after
	//hiting the player
	public var damageTimer = 0.0;
	
	public var distancePlayerNearby = 100.;
	
	
	
	public function new(x:Float, y:Float, state:Int, player:Player)
	{
		super(x, y, state, player);
		
		loadGraphic(AssetPaths.spear_enemy__png, true, WIDTH, HEIGHT);
		
		setFacingFlip(FlxObject.LEFT, false, false);
		setFacingFlip(FlxObject.RIGHT, true, false);
		facing = FlxObject.LEFT;
		
		animation.add("idle", [0]);
		animation.add("run", [0, 1, 2, 3], 8);
		animation.play("run");
		
		health = 65;
		damage = 20;
		
		currentSpeed = SPEED;
		
	}
	
	public function chooseWalkingDistance():Void
	{
		currentWalkingDistance = Util.randomBetween(minWalkingDistance, maxWalkingDistance);
	}
	
	override public function think(elapsed):Void
	{
		switch state
		{
			case Enemy.STATE_IDLE:
				if (isPlayerNearby(distancePlayerNearby))
				{
					state = Enemy.STATE_ATTACKING;
					velocity.set(0, 0);
					reachedGoal = true;
					return;
				}
				
				if (reachedGoal)
				{
					currentSpeed = SPEED;
					chooseWalkingDistance();
					if (facing == FlxObject.LEFT)
					{
						xGoal = x + currentWalkingDistance;
						directionToGo = 2;//right
					}
					else
					{
						xGoal = x - currentWalkingDistance;
						directionToGo = 1;//left
					}
					xGoal = FlxMath.bound(xGoal, 0, FlxG.worldBounds.width - WIDTH);
					reachedGoal = false;
				}
				else
				{
					if (directionToGo == 2)
					{
						goRight();
					}
					else if (directionToGo == 1)
					{
						goLeft();
					}
					
					reachedGoal = ((directionToGo == 1) && x <= xGoal) ||
						((directionToGo == 2) && x >= xGoal);
					
					
					
				}
				
			case Enemy.STATE_ATTACKING:
				if (!isPlayerNearby(distancePlayerNearby))
				{
					state = Enemy.STATE_IDLE;
					velocity.set(0, 0);
					return;
				}
				
				if (!attacking)
				{
					currentSpeed = ATTACK_SPEED;
					facing = if (x < player.x ) FlxObject.RIGHT else FlxObject.LEFT;
					targetX = player.midPoint.x;
					targetY = player.midPoint.y;
					
					var dx = player.midPoint.x - x;
					var dy = player.midPoint.y - y;
					
					var angle = Math.atan2(dy, dx);
					
					velocity.set(currentSpeed * Math.cos(angle), currentSpeed * Math.sin(angle));
					attacking = true;
					attackTimer = 0.0;
				}
				else if (attacking)
				{
					attackTimer += elapsed;
					if (attackTimer >= 1.0)
					{
						attackTimer = 1.0;
						attacking = false;
					}
					
					
				}
				
				
				
				
				
				
				
				
				
				
				
		}
	}
	
	public function attack():Void
	{
		
	}
	
	override public function update(elapsed:Float):Void
	{
		if (damageTimer > 0)
		{
			damageTimer -= elapsed;
			damageTimer = if (damageTimer < 0) 0 else damageTimer;
		}
		super.update(elapsed);
	}
	
	
	public function goLeft():Void
	{
		facing = FlxObject.LEFT;
		velocity.x = -1 * currentSpeed;
	}
	
	public function goRight():Void
	{
		facing = FlxObject.RIGHT;
		velocity.x = currentSpeed;
	}
	
	public function goUp():Void
	{
		velocity.y = -1 * currentSpeed;
	}
	
	public function goDown():Void
	{
		velocity.y = currentSpeed;
	}
	
}
