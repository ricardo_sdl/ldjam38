package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.group.FlxGroup;

class Util
{
	
	public static inline function randomBetween(min:Float, max:Float):Float
	{
		return Math.random() * (max - min) + min;
	}
	
	public static inline function equalFloats(a:Float, b:Float, precision:Float = 1 / 8192):Bool
	{
		return Math.abs(a - b) <= precision;
	}
	
	
	
}
