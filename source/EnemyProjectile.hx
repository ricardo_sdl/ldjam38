package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.math.FlxPoint;
import flixel.group.FlxGroup;

class EnemyProjectile extends FlxSprite
{
	
	public var damage:Float;
	
	public function new(x:Float, y:Float, _damage:Float)
	{
		super(x, y);
		damage = _damage;
	}
	
}
